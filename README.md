**Solution Overview**

The solution for this API was inspired by the DDD (domain-oriented design) approach.

And its development process was conducted through the TDD (Test-driven development) technique.

- Design pattern applied: Mediator.

#
**Note: The source code in the develop branch.**

#
**Main Technologies**

Node.js TypeScript Express.js Sequelize Jest Docker PostgreSQL Swagger Husky Commitlint Commitizen Eslint

#
**Instructions for running the project**

#
- Dependencies: Docker and Docker Compose
- Run application with: docker-compose up or docker-compose up -d

#
- Now with the SGDB of your choice, you should create a connection to the PostgreSQL database.
- Enter the database connection credentials according to the .env file.

#
- Enter in the application container: docker exec -it api sh 

# 
- Then run the following commands:
- $ yarn migrations
- $ yarn test

#
With online application homologate the API accessing the automatic documentation: [ Homologation API ](http://localhost:5000/ioasys-doc/v1/)


